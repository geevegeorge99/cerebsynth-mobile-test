/*
  
  Known bugs:
  1) Hiding elements causes UI issues
  
*/

let video;
const knnClassifier = ml5.KNNClassifier();

let featureExtractor;
let currentWord;
let myVoice;

let loadFlag = 0;

let classLimitBox;
let setLimitBtn; //setLimit
let totalNoOfClasses = 0;

let classDropDown; //dropdown
let classDropCtr = 0;

let classLabelBox; //labelBox
let setLabelBtn; //sets labelName
let classLabelCtr = 0;

let labelsArray = [];
let labelIndex = 0;

let labelBtn;
let labelBtnCtr = 0;
let labelCtr = 0;

let sampleCountBtn = 0;
let sampleCtr = [];
let cBtnCtr = 0;
let ctrVal = 0;

let resetBtn;
let resetBtnCtr = 0;

let ttsBtn;

var camState = (localStorage.getItem("camStateKey") === null) ? "user" : localStorage.getItem("camStateKey");

var camOptions = {
  video: {

    facingMode: {
      exact: camState
    }
  }
};

var ttsFlag = true;

function setup() {

  featureExtractor = ml5.featureExtractor('MobileNet', modelReady);
  noCanvas();

  video = createCapture(camOptions);
  video.size(200, 200);

  video.parent('videoContainer');

  createButtons();
  myVoice = new p5.Speech();
  myVoice.interrupt = true;
  
  classLimitBox = createInput();
  classLimitBox.parent('limitboxHolder');
  setLimitBtn = createButton('Set Limit');
  setLimitBtn.parent('limitboxHolder');
  setLimitBtn.class('limitBtn');
  
  classDropDown = createSelect();
  classDropDown.parent('classDropDownHolder');
  
  classLabelBox = createInput();
  classLabelBox.parent('labelInputBoxHolder');
  
  setLabelBtn = createButton('Set ' + classDropDown.value());
  setLabelBtn.parent('labelInputBoxHolder');
  setLabelBtn.class('limitBtn');
  
  labelBtn = createButton(labelsArray[classDropDown.value()]);
  labelBtn.parent('labelBtnHolder');
  labelBtn.class('themeBtn');
  
  sampleCountBtn = createButton(sampleCtr[classDropDown.value()]++);
  sampleCountBtn.parent('labelBtnHolder');
  sampleCountBtn.class('counter-count'); 
  
  resetBtn = createButton('Reset');
  resetBtn.parent('labelBtnHolder');
  resetBtn.class('themeBtn5');
  
  setLimitBtn.mousePressed(setClassesLimit);
  
}

function setClassesLimit() {

  totalNoOfClasses = classLimitBox.value();
  //initClassifier();

  if (totalNoOfClasses > 0) {

    classDropDown.html('');
    for (let i = 0; i <= totalNoOfClasses; i++) {
      if (i == 0)
        classDropDown.option('');
      else {
        classDropDown.option(i);
      }
    }

  }
  classDropDown.changed(setClassDetails);
}

function setClassDetails() {

  if (classDropDown.value() != '') {
    //setLabelBtn.show();
    //classLabelBox.show();

    
    if ((labelsArray.length != 0) && (typeof labelsArray[classDropDown.value()] !== 'undefined')) 
    {
      //if value already present     

      classLabelBox.value(labelsArray[classDropDown.value()]);
      setLabelBtn.html('Set ' + classDropDown.value());
      labelBtn.html(labelsArray[classDropDown.value()]);
      sampleCountBtn.html(sampleCtr[classDropDown.value()]);
 /*----------------------------------------------------------*/     
      setLabelBtn.mousePressed(saveLabelData);      
    }
    else 
    {

      classLabelBox.value('');
      setLabelBtn.html('Set ' + classDropDown.value());
      labelBtn.html('');
      sampleCountBtn.html('');

      setLabelBtn.mousePressed(saveLabelData);
    }
    
  }
  else
  {
    //setLabelBtn.hide();
    //classLabelBox.hide();
  }  
}

function saveLabelData() {
  
  labelIndex = classDropDown.value();
  if(labelIndex!='')
  {  
    labelsArray[labelIndex] = classLabelBox.value();
    createClassButtons();
  }
}



function modelReady() {
  select('#status').html('Ready to use!')
}

// Add the current frame from the video to the classifier
function addExample(label) {
  
  // Get the features of the input video
  const features = featureExtractor.infer(video);

  knnClassifier.addExample(features, str(label));
  //console.log(knnClassifier.getCountByLabel());
  updateCounts();
}

function classify() {
  // Get the total number of labels from knnClassifier
  const numLabels = knnClassifier.getNumLabels();
  
  if (numLabels <= 0) {
    console.error('There is no examples in any label');
    return;
  }
  // Get the features of the input video
  const features = featureExtractor.infer(video);

  knnClassifier.classify(features, gotResults);
}

function createClassButtons()
{

  labelBtn.html(labelsArray[classDropDown.value()]);
  labelBtn.mousePressed(createCounterReset);
  resetBtn.mousePressed(clrClassInfo);
}

function createCounterReset()
{

  addExample(labelsArray[classDropDown.value()]);
  ctrVal = sampleCtr[classDropDown.value()];
  
  if (typeof ctrVal == 'undefined') 
  {
    sampleCtr[classDropDown.value()] = 0;   
  }

  sampleCountBtn.html(sampleCtr[classDropDown.value()]++);
  
}
// A util function to create UI buttons
function createButtons() {
  
  // Predict button
  buttonPredict = select('#buttonPredict');
  buttonPredict.mousePressed(classify);
  
    // Save model
  saveBtn = select('#save');
  saveBtn.mousePressed(saveMyKNN);
  
  switchCameraBtn = select('#switchCamBtn');
  switchCameraBtn.mousePressed(switchCamera);
  
  ttsBtn = select('#switchTTSBtn');
  ttsBtn.html('Speech On');
  ttsBtn.mousePressed(switchTTS);

  // Load saved classifier dataset
  buttonSetData = select('#load');
  buttonSetData.mousePressed(loadMyKNN);
  
  // Clear all classes button
  buttonClearAll = select('#clearAll');
  buttonClearAll.mousePressed(clearAllLabels);
}

function switchTTS()
{
  ttsFlag = !ttsFlag;
  
  if(ttsFlag==true)
    ttsBtn.html('Speech On');
  else
    ttsBtn.html('Speech Off');
    
}


function clrClassInfo()
{
  //console.log(labelsArray[classDropDown.value()]);
  knnClassifier.clearLabel(labelsArray[classDropDown.value()]);
  updateCounts();
  //console.log(sampleCtr[classDropDown.value()]);
  sampleCountBtn.html(sampleCtr[classDropDown.value()]);
}

function switchCamera()
{
  
  var r = confirm("Switching camera refreshes the page, so all information will be lost!");
  if (r == true) 
  {
      if (camState == "user") {
      //sets state
      camState = "environment";
      localStorage.setItem("camStateKey", camState);
      window.location.reload();
      //refresh page and assign 

    } else {
      camState = "user";
      localStorage.setItem("camStateKey", camState);
      window.location.reload();

    }  
        
  }  
  
}
// Save dataset as myKNNDataset.json
function saveMyKNN() {
  loadFlag = 0;
  
  knnClassifier.save('myKNNDataset');
}



// Show the results
function gotResults(err, result) {
  
  // Display any error
  if (err) {
    console.error(err);
  }

  if (result.confidencesByLabel) {
    
    const confidences = result.confidencesByLabel;
    // result.label is the label that has the highest confidence
    if (result.label) 
    {

      if(loadFlag==1)
      {  
        select('#result').html(knnClassifier.mapStringToIndex[int(result.label)]);
        select('#confidence').html(`${confidences[knnClassifier.mapStringToIndex[int(result.label)]] * 100} %`);
      }
      else
      {  
        select('#result').html(result.label);
        select('#confidence').html(`${confidences[result.label] * 100} %`);
      }
      // If the confidence is higher then 0.9
      if (result.label !== currentWord && confidences[result.label] > 0.9) {
        currentWord = result.label;
        // Say the current word 
        if (currentWord != "Background") {
          //reserved for base case
          if(ttsFlag==true)
            myVoice.speak(currentWord);
        }

      }
    }
    
  }

  classify();
}

// Update the example count for each class	
function updateCounts() {
  const counts = knnClassifier.getCountByLabel();

  if(typeof counts[labelsArray[classDropDown.value()]] === 'undefined')
    counts[labelsArray[classDropDown.value()]] = 0;

  //<bug> could save memory here by only going through loop when clearAll is issued  

  //sampleCtr[classDropDown.value()] = counts[labelsArray[classDropDown.value()]];
  
  for(let j=1;j<=totalNoOfClasses;j++)
  {
    sampleCtr[j] = counts[labelsArray[j]];
  }
  
}

// Clear all the examples in all classes
function clearAllLabels() {
  knnClassifier.clearAllLabels();
  updateCounts();
  sampleCountBtn.html(sampleCtr[classDropDown.value()]);
}

// Load dataset to the classifier
function loadMyKNN() {
  loadFlag = 1;
  knnClassifier.load('./myKNNDataset.json', updateCounts);
}
